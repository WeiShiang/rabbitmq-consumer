package com.example.rabbitmqconsumer.config;

import com.rabbitmq.client.ConnectionFactory;
import com.example.rabbitmqconsumer.dto.OrderDTO;
import com.example.rabbitmqconsumer.handler.MessageDelegate;
import com.example.rabbitmqconsumer.handler.TextMessageConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
import org.springframework.amqp.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan(basePackages = {"com.example.rabbitmqconsumer"})
public class RabbitMQConfig {
    private static Logger log = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.port}")
    private int port;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.listener.simple.max-concurrency}")
    private int maxConcurrency;

    @Value("${spring.rabbitmq.listener.simple.concurrency}")
    private int concurrency;

    @Value("${spring.rabbitmq.listener.simple.retry.initial-interval}")
    private long retryInterval;

    @Value("${spring.rabbitmq.queue}")
    private String queueNames;

    @Bean
    public ConnectionFactory connectionFactory() throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
//        connectionFactory.setVirtualHost(env.getProperty("mq.vhost").trim());
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        log.debug(String.format("log.debug connect RabbitMQ %s:%s success...", host, port));
        return connectionFactory;
    }

    @Bean
    public CachingConnectionFactory cachingConnectionFactory() throws Exception {
        return new CachingConnectionFactory(connectionFactory());
    }

    @Bean
    public SimpleMessageListenerContainer listenerContainer() throws Exception {


        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer(cachingConnectionFactory());
        //監聽複數個queue "test, test1, test2"
        simpleMessageListenerContainer.setQueueNames(queueNames.split(","));
        simpleMessageListenerContainer.setDefaultRequeueRejected(false);
        simpleMessageListenerContainer.setMaxConcurrentConsumers(maxConcurrency);
        simpleMessageListenerContainer.setConcurrentConsumers(concurrency);
        simpleMessageListenerContainer.setRetryDeclarationInterval(retryInterval);

        ContentTypeDelegatingMessageConverter convert = new ContentTypeDelegatingMessageConverter();
        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageDelegate());
        adapter.setDefaultListenerMethod("onMessage");

        //指定Json轉換器
        Jackson2JsonMessageConverter jackson2JsonMessageConverter =new Jackson2JsonMessageConverter();
        DefaultJackson2JavaTypeMapper jackson2JavaTypeMapper = new DefaultJackson2JavaTypeMapper();
        //put json object
        Map<String,Class<?>> idClassMapping = new HashMap<String, Class<?>>();
        idClassMapping.put("order", OrderDTO.class);
        jackson2JavaTypeMapper.setIdClassMapping(idClassMapping);

        jackson2JsonMessageConverter.setJavaTypeMapper(jackson2JavaTypeMapper);
        Arrays.asList("json", "application/json")
                .forEach(type -> convert.addDelegate(type, jackson2JsonMessageConverter));

        //text轉換器
        TextMessageConverter textConvert =  new TextMessageConverter();
        Arrays.asList("text", "html/text", "xml/text", "text/plain")
                .forEach(type -> convert.addDelegate(type, textConvert));

        //圖檔轉換器
//        Arrays.asList("image/jpg", "image/jepg", "image/png", "text/plain")
//                .forEach(type -> convert.addDelegate(type, new JPGMessageConverter()));

        adapter.setMessageConverter(convert);
        simpleMessageListenerContainer.setMessageListener(adapter);
        simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
        return simpleMessageListenerContainer;
    }

}
